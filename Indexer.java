/* John Stevenson
CSE 223 
PA2
The Indexer class is used to read a file and create a hashmap containing the words of the file and where they occur in the file. 
Public methods include processFile(), numberOfInstances(), numberOfWords(), toString(), and locationOf().  
*/
import java.util.Scanner;
import java.lang.String;
import java.util.LinkedList;
import java.io.File;
import java.util.HashMap;

public class Indexer
{
	private HashMap<String,LinkedList<Integer>> index = new HashMap<String,LinkedList<Integer>>(); //index is a hashmap containing the words of the file as keys and their locations in a linked list of integers
	private Scanner sc; //sc is a scanner used to open and read the file given to Indexer
	private boolean init = false; //init is used to tell whether or not a file has been read; default value is false 
	//private Integer position=1; //position is the index of the current word in the file
 
	/* 
	This is the Indexer constructor.
	It does nothing in particular.
	*/
	public void Indexer()
	{
	}

	/*
	processFile is a public method of the Indexer class.
	It is used to read a file and index the words within 
	it using a hashmap. When run successfully, index will 
	contain all words in the file in uppercase and init 
	will be set to true. It will get rid of all punctuation 
	and non-alphabetic characters. Returns true when run 
	successfully and false if the file was not found or unreadable. 
	Takes an argument of type String which should be the path 
	to the file you wish to process.
	*/

	public boolean processFile(String file)
	{
		try 
		{
			sc = new Scanner(new File(file)); //tries to open the file passed to processFile 
		}
		catch(Exception e) //if the file is unreadable or does not exist, return false
		{
			return (false); //return false to signal a failed operation 
		}
		
		int position = 1; //word position of the scanner in the file
		String word; //a string containing the next word in the file

		while(sc.hasNext()) //while there is still something in the file
		{
			word = sc.next(); //move forward one word
			word = cleanupWord(word); //gets rid of any punctuation or non-alphabetic characters in the word
			if(!word.isEmpty()) //if the word is empty, do nothing
			{
				addReference(word,position); //adds the word to the hashmap
				position++;
			}
		}	
		init = true; //the file has been processed 
		return (true); //return true to signal a successful operation
	}	
	
	/*
	cleanupWord is a private method of the Indexer class.
	It is passed a String and returns the uppercase 
	version of the String with only alphabetic characters.
	If the string has no alphabetic characters, it will
	return an empty String.
	*/
	
	private String cleanupWord(String word)
	{
		String output = ""; //output string initialized to an empty string

		for(int i = 0; i < word.length(); i++) //move through the String passed to the method 
		{
			if((word.charAt(i) >= 'a' && word.charAt(i) <= 'z')||(word.charAt(i) >= 'A' && word.charAt(i) <='Z')) //if the character is alhpabetic, add it to the output string
			{
				output = output + word.charAt(i); //appends the current letter to the output string
			}
		}

		return output.toUpperCase(); //returns the uppercase version of the output string			 
	}

	/* 
	addReference is a private method of the Indexer class.
	It takes a word and its position in the file that is 
	currently being read. If the word is not empty, it
	is added to the hashmap and a new linked list is 
	created. If the entry is already in the hashmap,
	the position is appended to the linked list 
	associated with the word. The word passed to this 
	function should be processed before being passed.
	*/
	private void addReference(String word,int position)
	{
		if(index.get(word)==null) //if the word is not already in the hashmap, add it to the hashmap
		{
			index.put(word, new LinkedList<Integer>()); //adds the word to the hashmap and creates a new linked list associated with that entry
		}
		index.get(word).add(position); //adds the position of the word to the linked list associated with that word
		return;
	}	
	
	/*
	numberOfInstances is a public method of the Indexer class.
	It takes a word and returns the number of times that word
	occurs in the file. If the file has not yet been processed,
	this function returns -1. If the word is not in the file, 
	the return value is 0. Otherwise, the return value is the 
	number of instances of that word in the file.   
	*/
	public int numberOfInstances(String word)
	{
		if(!init) //if the file has not yet been processed, return -1 
		{
			return (-1);
		}

		LinkedList list = index.get(word); //list is a 
		
		if(list==null) //if the list doesn't contain anything, return 0
		{
			return (0);
		}

			
		return (list.size()); //return the number of times the word appears in the file
	}

	/*
	locationOf is a public method of the Indexer class
	It takes a word and the requested instance of the 
	word and returns the location of that instance. 
	If the file has not yet been processed, it returns
	-1. If the instance number does not exist, it returns 
	-1. 
	*/
	public int locationOf(String word,int instanceNum)
	{
		if(!init) //if the file has not been processed, return -1
		{
			return (-1); //return -1 signalling failed operation
		}		

		try
		{
			return(index.get(word).get(instanceNum)); //tries to return the location of a certain instance of a word
		}
		catch(Exception e) //if the instance does not exist
		{
			return (-1); //return -1 signalling a failed operation
		}
	}

	/*
	toString is a public method of the Indexer class.
	It returns the value of the hashmap. If the file
	has not yet been processed, it returns null.
	*/
	public String toString()
	{
		if(!init) //if the file has not been processed, return null
		{ 
			return (null); //return null signalling a failed operation
		}
		
		return (index.toString()); //return the value of the hashmap
	}
	
	/*
	numberOfWords is a public method of the Indexer class.
	It returns the number of unique words in the file. If 
	the file has not yet been processed, it returns -1. 
	*/
	public int numberOfWords()
	{
		if(!init) //if the file has not been processed, return -1
		{
			return (-1); //return -1 signalling a failed operation
		}
		return(index.size()); //return the number of unique words in the file
	}
}
